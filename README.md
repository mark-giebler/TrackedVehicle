# Giebler's Auto-Drive Tracked Vehicle

Our arduino controlled, auto-driving tracked vehicle code.

Uses the following devices:
+ **[TAMIYA 70168]** Dual DC Motor drive train 
+ **[L298]** Dual H-Bridge drive electronics 
+ **[HC-SR04]** Ultrasonic range sensor
+ **[OPTEK OPB12391]** Opto-Mechanical bump sensor
+ **[HC-06]** Bluetooth to Serial module _(for telemetry)_
+ **[TSOP38438]** IR Receiver _(for manual driving)_

That's it for now.
