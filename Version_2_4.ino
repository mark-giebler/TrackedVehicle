// The Giebler's auto-drive robotic tracked vehicle
//
// Project on GitLab: https://gitlab.com/mark-giebler/TrackedVehicle
// Master Git Branch: https://gitlab.com/mark-giebler/TrackedVehicle/tree/master
//
// Board type: Arduino Uno
//
// Version History:
// v 2.40
// 	- add menu on '?'
//  - add left / right from IR control.
//		if Thr: > 20  and: F/R: 3 And Dir: > 80 --> Left  Dir: < 40 --> Right  
//
//
// v 2.30
//	- added Serial input from Bluetooth for control
//		Z/X/A/a - Start autoDrive /  z/x - Stop
//	Use the bluetooth RC android app.  https://play.google.com/store/apps/details?id=braulio.calle.bluetoothRCcontroller
// These are the characters used:
		/*
		Halt-----------------------D
		Forward---------------------F
		Back-------------------------B
		Left---------------------------L
		Right-------------------------R
		Forward Left--------------G
		Forward Right------------I
		Back Left------------------H
		Back Right----------------J
		Stop-------------------------S
		Front Lights On---------W
		Front Lights Off---------w (lower case)
		Back Lights On---------U
		Back Lights Off---------u (lower case)
            Speaker On  -------------V
            Speaker Off -------------x
		Extra control On --------X
		Extra control Off -------x
		Speed 0-------------------0
		Speed 10-----------------1
		Speed 20-----------------2
		Speed 30-----------------3
		Speed 40-----------------4
		Speed 50-----------------5
		Speed 60-----------------6
		Speed 70-----------------7
		Speed 80-----------------8
		Speed 90-----------------9
		Speed 100---------------q
		Everything OFF--------D
		*/
// v 2.20
//	- added potentiometer motor A/B balance control.
//
// v 2.10
//	- added IR command HEX dump out serial port
// v 2.00
//	- Start adding in IR control using Mark's IR Lib IR_heli_lib.ino.
// v 1.10
//	- added battery voltage reporting out serial port
//  - cleaned up state machine enum names.
//	- added a few more comments
// v 1.09
//	- Decreased object stop time by 100 mSec.
//	- decreased turn count toggle from 30 to 20
//	- minor change to object detect slow down rate.
// v 1.08
//	- added slow down when object detected 35 cm away.
//	- changed turn count toggle code to change turn direction after 30 turns.
//	- added more comments
// v 1.07
//	- added turn count toggle code to change turn direction after 35 turns or 48 sec going straight.
//	- added status reporting out serial port
//	- increased turn time on bump sensor event
//	- increased back up time on bump sensor event
//	- increased object stop time from 250 to 640 mSec to allow inertia in system to dissipate.
// v 1.06
//	- added different turn time for ultrasonic detect and bump detect.
//	- cleaned up code to use more macros
//
#define VERSION_GEEB "v2.42 "
#define STARTUP_DELAY 1800	/* mSec give us time to connect power and put robot down. */

#define DX_TRIGGER	20    /* cm away for detect object stop.  Note: @ 22 was detecting floor sometimes. */
#define DX_SLOW		35	  /* cm away for start slowing down */

void printStatus(uint8_t force = 0);
// -----------------------------------------------
// IO Pins
// Define our pin mappings for how we have our external hardware devices connected to
// our Arduino board.

#define MOTOR_SKEW	0	// Analog Potentiometer input to adjust Motor A/B speed delta
#define BATTERY_RAW 1	// Analog battery input.
const int TrigPin = 2;	// Trigger pin for ultrasonic range sensor
const int EchoPin = 3;	// Echo response from ultrasonic range sensor
#define BUMP_SENSOR 4	// Digital bump input pin 
#define MOTOR_A_PWM 5	// L298 H-Bridge
#define MOTOR_B_PWM 6
#define MOTOR_A_DIRECTION 7
#define MOTOR_B_DIRECTION 8
#define KILL_SWITCH 9	// Kill switch input
#define IR_RX	11		// IR RX input pin
#define LED2 13

// -----------------------------------------------
// Define some macros for frequently used constant values
#define MOTOR_FORWARD 	1
#define MOTOR_BACKWARD 	0
#define MOTOR_STOP 		0
#define MOTOR_A_SPEED 	80 // high number is faster
#define MOTOR_B_SPEED 	80 // high number is faster

// Define some time delays, units mSec.
#define TURN_TM_NORMAL 	(410)
#define TURN_TM_BUMP	(700)
#define BACKUP_TM_NORMAL (860)	// ultra sound detect backup time.
#define BACKUP_TM_BUMP	(2*BACKUP_TM_NORMAL)  // when we bump something backup further
#define OBJECT_STOP_TM	(540)
#define BACK_STOP_TM	(540)

#define TOGGLE_DIR_TM	(48000)	/* toggle turn direction every xx seconds  of going straight.*/
#define TOGGLE_DIR_COUNT	(20) /* toggle turn direction every N turns. */

// -----------------------------------------------------
// Mark's debug macro
// -----------------------------------------------------
// 		mjg: put any debug only code inside the parenthesis (represented by 'a') in below macro.
//  	'a' can be multiple statements on multiple lines.
// 		comment out next line to turn off all debug code.
#define G_DEBUG(a)  a;
#ifndef G_DEBUG
// 		if G_DEBUG() is commented out above, then define NULL debug macro that throws code 'a' away.
#define G_DEBUG(a) ;;;
#endif
// 		use this macro to temporarily disable an individual G_DEBUG() macro group of code by renaming it to this:
#define G_DEBUG_N(a) ;;;
// -----------------------------------------------------
// END -- Mark's debug macro
// -----------------------------------------------------

// -----------------------------------------------
// enumerate state machine states
enum ourstates
{
  Halt_St,			// stop and hang.
  Auto_Start_St,			// used to exit halt state and start auto drive.
  Auto_Fwd_St,
	Fwd_St,
  Auto_ObjStp_St,
    ObjStp_St,
  Auto_Bwd_St,
	Bwd_St,
  Auto_BwdStp_St,
	BwdStp_St,
  Auto_TrnRight_St,
	TrnRight_St,
	TrnRightFwd_St,
	TrnRightBwd_St,
  Auto_TrnLeft_St,
	TrnLeft_St,
	TrnLeftFwd_St,
	TrnLeftBwd_St,
  Manual_St,
  Error_St
};
volatile ourstates state = Auto_Start_St;
volatile ourstates lastState = Halt_St;
volatile ourstates nextState = Auto_Start_St;

volatile ourstates turnDirection = Auto_TrnRight_St;
uint16_t turnTime = TURN_TM_NORMAL;	// how much time to spend in a turn.
uint8_t	turnCount;					// count turns, after TOGGLE_DIR_COUNT turns, change turn direction.
uint16_t backupTime = BACKUP_TM_NORMAL; // how much time to spend backing up.
uint32_t lastIRcmd;
uint32_t IRcmd;

// Current speed of motors.
int speed_motor_a;
int speed_motor_b;
// Maximum speed of motors.
int speed_motor_a_max;
int speed_motor_b_max;

void set_motor_a_max_speed(int s) { if(s>0) speed_motor_a_max=s; else speed_motor_a_max=0;}
void set_motor_b_max_speed(int s) { if(s>0) speed_motor_b_max=s; else speed_motor_b_max=0;}
int get_motor_a_max_speed() {return speed_motor_a_max;}
int get_motor_b_max_speed() {return speed_motor_b_max;}

//uint16_t getState(){ return state;}

unsigned long currentTime;	// so only one call to millis() is needed.
unsigned long dirTogElapsedTime;	// after TOGGLE_DIR_TM seconds of no turns, switch turn direction.
unsigned long lastDxTime;	// to determine when to measure distance
unsigned long lastBatTime;	// read battery timer
unsigned long ledTogTime;	// LED toggle timer

int lastDX;					// keep track of distance measurement for status reporting.
unsigned int lastBattery = 6500;	// voltage x 1000
signed int lastSkewRawAvg = 512;
signed int lastSkewMtrVal = 32;

uint8_t	rxCommand = 0;		// serial port (bluetooth) command.
uint8_t lastRxCommand = 0;

uint8_t led2state = LOW;
uint8_t autoMode = 0;		// 1 == auto drive mode active.

// -----------------------------------------------
// setup()
//
void setup() {
  // put your setup code here, to run once:
  Serial.begin(57600);
  Serial.println(F("\n-Start-\n"));
  Serial.println(VERSION_GEEB);

  pinMode(KILL_SWITCH, INPUT_PULLUP);
  pinMode(LED2, OUTPUT);
  digitalWrite( LED2, LOW);  // LED off.

  pinMode(TrigPin, OUTPUT);
  pinMode(EchoPin, INPUT_PULLUP);

  pinMode(MOTOR_A_DIRECTION, OUTPUT);
  digitalWrite(MOTOR_A_DIRECTION, MOTOR_STOP);
  pinMode(MOTOR_B_DIRECTION, OUTPUT);
  digitalWrite(MOTOR_B_DIRECTION, MOTOR_STOP);
  pinMode(MOTOR_A_PWM, OUTPUT);
  digitalWrite(MOTOR_A_PWM, MOTOR_STOP);
  pinMode(MOTOR_B_PWM, OUTPUT);
  digitalWrite(MOTOR_B_PWM, MOTOR_STOP);

  pinMode (BUMP_SENSOR, INPUT_PULLUP);

  currentTime = lastDxTime = lastBatTime = ledTogTime = millis();

  dirTogElapsedTime = currentTime + TOGGLE_DIR_TM;
  turnCount = 0;
  lastDX = 401;

  // setup motor speed values.
  set_motor_a_max_speed(MOTOR_A_SPEED);
  set_motor_b_max_speed(MOTOR_B_SPEED);
  speed_motor_a = get_motor_a_max_speed();
  speed_motor_b = get_motor_b_max_speed();
  
  // Set initial value of integrator variable for voltage.
  lastBattery = 0;
  BatteryVoltage();
  lastBattery *= 10;
  lastSkewRawAvg = ReadMotorSkewAdjust();
  lastSkewMtrVal = ((512 - lastSkewRawAvg)/16);
  
  // initialize IR receive
  ir_setup();
  ir_startRx();		// start ir state machine
}

// ----------------------------------------------
// set motor direction of  motor A
// dir can be: MOTOR_FORWARD or MOTOR_BACKWARD
void motor_A_dir(int dir, int speed)
{
	if( speed <0 ) speed = 0;
  //  int speed=MOTOR_A_SPEED;
  digitalWrite(MOTOR_A_DIRECTION, dir);
  if (dir == MOTOR_FORWARD)
    speed = 255 - speed;
  analogWrite(MOTOR_A_PWM, speed);
}

// -----------------------------------------------
// set motor direction of  motor B
// dir can be: MOTOR_FORWARD or MOTOR_BACKWARD
void motor_B_dir(int dir, int speed)
{
	if( speed <0 ) speed = 0;

  //  int speed=MOTOR_B_SPEED;
  digitalWrite(MOTOR_B_DIRECTION, dir);
  if (dir == MOTOR_FORWARD)
    speed = 255 - speed;
  analogWrite(MOTOR_B_PWM, speed);
}
// -----------------------------------------------

void motor_A_stop()
{
  digitalWrite(MOTOR_A_DIRECTION, MOTOR_STOP);
  analogWrite(MOTOR_A_PWM, MOTOR_STOP);
}
// -----------------------------------------------

void motor_B_stop()
{
  digitalWrite(MOTOR_B_DIRECTION, MOTOR_STOP);
  analogWrite(MOTOR_B_PWM, MOTOR_STOP);
}

// -----------------------------------------------
uint8_t checkKillSwitch()
{
  uint8_t kill = 0;
  if (digitalRead(KILL_SWITCH) == 0)
  { // switch pressed
    // debounce and ignore short pulses
    delay(100);
    if (digitalRead(KILL_SWITCH) == 0)
    {
      // if kill switch pressed..
      kill = 1;
    }
  }
  return kill;
}
void ManualDrive(void)
{
  
}
// print menu of direction commands
void printMenu()
{
	Serial.println( F("D/z - Halt       S - Stop") );
	Serial.println( F("F - Forward      B - Backward") );
	Serial.println( F("L - Left         R - Right") );
	Serial.println( F("A - AutoDrive    x - Exit Auto") );
}
// ----------------------------------------------
// Main operation loop
// ----------------------------------------------
void loop() {
  uint8_t ir_thr;
  uint8_t ir_xVec;
  uint8_t ir_yVec;
  //Serial.print ("st: ");
  //Serial.println (state);

  state = nextState;
  
  // -------------------------------
  // Check inputs
  // -------------------------------
  // always check kill switch
  if (checkKillSwitch())
  {
    // halt  if kill switch.
    nextState = Halt_St;
	digitalWrite( LED2, HIGH);
	while(checkKillSwitch())
		;			// wait for switch release
	delay(300);
  }

  // serial input 
  if(Serial.available() > 0){
    int speedVal;
    rxCommand = Serial.read();
    if(rxCommand >= ' ' )
    { 	// ignore /r /n 
		Serial.println((char)rxCommand);
		switch(rxCommand)
		{
			case '?':		// give status dump out.
				printStatus(1);
				printMenu();
				break;
			
			case 'z':		// Halt
			case 'x':
			case 'D':
				nextState = Halt_St;
				digitalWrite( LED2, HIGH);
				break;
			
			case 'Z':	// auto drive if halt state.
			case 'a':
				if(state == Halt_St)
					nextState = Auto_Start_St;
				break;
			case 'X':	// auto drive in any state
			case 'A':
				nextState = Auto_Start_St;
				break;
				
			case 'S': 
			  nextState = ObjStp_St;
			  break; 
			case 'F':  // Forward
				nextState = Fwd_St;
				break;
			case 'B':  // Backward
				nextState = Bwd_St;
				break;
				
			case 'R':  // Right
				nextState = TrnRight_St;
				break;
			case 'I':  //FR fwd rt
				nextState = TrnRightFwd_St;
				break;
			case 'J':  //BR 
			  //yellowCar.BackRight_4W(velocity);
			  nextState = TrnRightBwd_St;
			  break;       
			
			case 'L':  // Left 
				nextState = TrnLeft_St;
			  break;
			case 'G':  //FL 
			  nextState = TrnLeftFwd_St;
			  break;
			case 'H':  //BL
			  //yellowCar.BackLeft_4W(velocity);
			  nextState = TrnLeftBwd_St;
			  break;
			//case '0':
			default:
				// check if speed value
				if(rxCommand > '0' && rxCommand <= '9')
				{	// we want speed range of 45 to 90
					speedVal = ((rxCommand - '0') * 5) + 45;
					set_motor_a_max_speed( speedVal );
					set_motor_b_max_speed( speedVal );
				}
				break;
		}
    }
  }
  
  printStatus();		// output current status if any change from last report.

  // ------------------------------
  // House keeping tasks that are done periodically
  // ------------------------------
  currentTime = millis();
  if ( currentTime > (lastBatTime + 500))
  { // time to update battery voltage.
    BatteryVoltage();
    lastBatTime = currentTime;
	
	// read the adjust motor skew pot
	ReadMotorSkewAdjust();
	lastSkewMtrVal = ((512 - lastSkewRawAvg)/16);
  }

  if (turnCount > TOGGLE_DIR_COUNT)
  { // time to change turn direction.
    if ( turnDirection == Auto_TrnLeft_St )
    {
      turnDirection = Auto_TrnRight_St;
    } else
    {
      turnDirection = Auto_TrnLeft_St;
    }
    turnCount = 0;
  }
  
  if ( currentTime > (lastDxTime + 250) )
  {
     // check ultrasonic DX every 250 mSec
     lastDX = Distance();
	 lastDxTime = currentTime;
  }
  // -------------------------------
  // look for any IR commands
  // -------------------------------
  IRcmd = ir_processCommand();

  if ( IRcmd != lastIRcmd)
  {
	// have a new command, so process it.
    lastIRcmd = IRcmd;
    if ( ir_ready() )
    {
      ir_startRx();
		ir_thr = get_throttle();
      if ( ir_thr > 20)
      {
        nextState = Manual_St;
        Serial.print( "MD " );
		ir_xVec = get_Xvector(); // fwd /rev
		ir_yVec = get_Yvector(); // left / right
		//		if Thr: > 20  and: F/R: 3 And Dir: > 80 --> Left  Dir: < 40 --> Right  
		if( ir_xVec == 3 ){
			// spin left / right
			if( ir_yVec < 40 )
				nextState = TrnRight_St;
			else if(ir_yVec > 80)
				nextState = TrnLeft_St;
			else
				nextState = ObjStp_St;	// stop.
		}else if( ir_xVec > 4)
		{
			// turn left / right or straight fwd.
			if( ir_yVec < 40 )
				nextState = TrnRightFwd_St;
			else if(ir_yVec > 80)
				nextState = TrnLeftFwd_St;	
			else 
				nextState = Fwd_St;
		}else if( ir_xVec < 2)
		{
			nextState = Bwd_St;	// straight back.
		}
      }
      else if( get_channel() == 1 )
      {	// if chn: A  then auto drive when throttle goes low.
        nextState = Auto_Start_St;
        lastIRcmd = 0;
      }
      G_DEBUG(
		Serial.print( F("  ") );
		Serial.print( F("IR ") );
		Serial.print( IRcmd, HEX );
		Serial.print( F("   ") );			
		Serial.print( F("Thr:") );
		Serial.print( get_throttle() );
		Serial.print( F("  F/R:") );
		Serial.print( get_Xvector() );
		Serial.print( F("  Dir:") );
		Serial.print( get_Yvector() );
		Serial.print( F("  Chn:") );
		Serial.print( get_channel() );
		Serial.println();
      )
      
    }
    else if ( ir_error() )
    {
      G_DEBUG(
      	Serial.print( F("IR ") );
	Serial.print( IRcmd, HEX );
	Serial.println( F(" <<-Error  ") );
      );
      ir_startRx();
    }
    else if ( !ir_idle() && !ir_busy() )
    {
      G_DEBUG(
      Serial.print( F("IR Unknown ") );
      Serial.print( IRcmd, HEX );
      Serial.print( F("  St: ") );
      Serial.println( get_ir_state() );
      );
      ir_startRx();
    }
  } else
  {
    G_DEBUG_N(
      Serial.print( F("Same ") );
      Serial.print( IRcmd, HEX );
      Serial.print( F("  St: ") );
      Serial.println( get_ir_state() );
    );
  }
  // ------------------------------------
  //	State Machine Handler
  // 	handle events for current state.
  //
  // ------------------------------------
  switch (state)
  {
    case Auto_Start_St:
      Serial.println( F("Auto_Start_St: ") );
      delay( STARTUP_DELAY );
      nextState = Auto_Fwd_St;
      dirTogElapsedTime = currentTime + TOGGLE_DIR_TM;
      lastDX = 0;		// set DX so it is reported.
      lastDxTime = 0;
	  autoMode = 1;
		// reset motor speeds for auto drive.
	    set_motor_a_max_speed(MOTOR_A_SPEED);
		set_motor_b_max_speed(MOTOR_B_SPEED);
      break;

    case Manual_St:
	  autoMode = 0;
      ManualDrive();	// processes any input commands???
      break;

    case Auto_Fwd_St:
      if ( currentTime > dirTogElapsedTime  )
      { // toggle turn direction and reset turn count.
        dirTogElapsedTime = currentTime + TOGGLE_DIR_TM;

        if ( turnDirection == Auto_TrnLeft_St )
        {
          turnDirection = Auto_TrnRight_St;
        } else
        {
          turnDirection = Auto_TrnLeft_St;
        }
        if (turnCount > 1)
          turnCount -= 2;
      }
      if (digitalRead(BUMP_SENSOR) == 1)
      {
        // stop right away if bump detected.
        nextState = Auto_ObjStp_St;
        turnTime = TURN_TM_BUMP;
        backupTime = BACKUP_TM_BUMP;
        lastDX = 0;		// set DX so it is reported.
        return;
      }
//      if ( currentTime > (lastDxTime + 250) )
      {
        // check ultrasonic DX every 250 mSec
//      lastDX = Distance();
//		lastDxTime = currentTime;
		
        if ( lastDX < DX_TRIGGER )
        {
          nextState = Auto_ObjStp_St;
          turnTime = TURN_TM_NORMAL;
          backupTime = BACKUP_TM_NORMAL;
          digitalWrite( LED2, HIGH);
          return;
        }
        if ( lastDX < DX_SLOW )
        { // slowly approach objects
          if ( speed_motor_a > 55 )
            speed_motor_a -= 4;
          if ( speed_motor_b > 55 )
            speed_motor_b -= 4;
        } else
        {
          speed_motor_a = get_motor_a_max_speed() - lastSkewMtrVal;
          speed_motor_b = get_motor_b_max_speed() + lastSkewMtrVal;
        }

        { // keep going forward...
          digitalWrite( LED2, LOW);
          motor_A_dir( MOTOR_FORWARD, speed_motor_a);
          motor_B_dir( MOTOR_FORWARD, speed_motor_b);
        }
        
      }
      break;
	  
	case Fwd_St:
		{ // keep going forward...
		  speed_motor_a = get_motor_a_max_speed() - lastSkewMtrVal;
          speed_motor_b = get_motor_b_max_speed() + lastSkewMtrVal;
          digitalWrite( LED2, LOW);
          motor_A_dir( MOTOR_FORWARD, speed_motor_a);
          motor_B_dir( MOTOR_FORWARD, speed_motor_b);
        }
	  break;
	  
    case Auto_ObjStp_St:
      // stop after object detected or bumped.
      motor_A_stop();
      motor_B_stop();
      delay( OBJECT_STOP_TM );
      nextState = Auto_Bwd_St;
      dirTogElapsedTime = currentTime + TOGGLE_DIR_TM; // reset forward elapsed time.
      speed_motor_a = get_motor_a_max_speed() - lastSkewMtrVal;
      speed_motor_b = get_motor_b_max_speed() + lastSkewMtrVal;
      break;

	case ObjStp_St:
      // stop 
      motor_A_stop();
      motor_B_stop();	
	  // reset motor speed in case of turns.
	  speed_motor_a = get_motor_a_max_speed() - lastSkewMtrVal;
      speed_motor_b = get_motor_b_max_speed() + lastSkewMtrVal;
	  break;
	  
    case Auto_Bwd_St:
      motor_A_dir( MOTOR_BACKWARD, speed_motor_a);
      motor_B_dir( MOTOR_BACKWARD, speed_motor_b);
      delay( backupTime );
      nextState = Auto_BwdStp_St;
      digitalWrite( LED2, LOW);
      break;

	case Bwd_St:
	  speed_motor_a = get_motor_a_max_speed() - lastSkewMtrVal;
      speed_motor_b = get_motor_b_max_speed() + lastSkewMtrVal;
	  motor_A_dir( MOTOR_BACKWARD, speed_motor_a);
      motor_B_dir( MOTOR_BACKWARD, speed_motor_b);
	  break;
	  
    case Auto_BwdStp_St:
      // stop after backing up and before initiating a turn
      motor_A_stop();
      motor_B_stop();
      delay( BACK_STOP_TM );
      nextState = turnDirection;
      break;

    case Auto_TrnRight_St:
      motor_B_dir( MOTOR_BACKWARD, speed_motor_b);
      motor_A_dir( MOTOR_FORWARD, speed_motor_a);
      delay( turnTime );
      nextState = Auto_Fwd_St;
      turnCount++;
      break;

	case TrnRight_St:	// slow turn to right
      motor_B_dir( MOTOR_BACKWARD, speed_motor_b);
      motor_A_dir( MOTOR_FORWARD, speed_motor_a);
	  break;
	case TrnRightFwd_St:	// slow turn to right
      motor_B_dir( MOTOR_FORWARD, speed_motor_b - speed_motor_b/3);
      motor_A_dir( MOTOR_FORWARD, speed_motor_a);
	  break;
	case TrnRightBwd_St:  // slow turn to right in reverse
      motor_B_dir( MOTOR_BACKWARD, speed_motor_b - speed_motor_b/3);
      motor_A_dir( MOTOR_BACKWARD, speed_motor_a);
	  break;
	
	  
    case Auto_TrnLeft_St:
      motor_A_dir( MOTOR_BACKWARD, speed_motor_a);
      motor_B_dir( MOTOR_FORWARD, speed_motor_b);
      delay( turnTime );
      nextState = Auto_Fwd_St;
      turnCount++;
      break;
	  
   case TrnLeft_St:	// slow turn to left
      motor_A_dir( MOTOR_BACKWARD, speed_motor_a);
      motor_B_dir( MOTOR_FORWARD, speed_motor_b);
	  break;
   case TrnLeftFwd_St:	// slow turn to left
      motor_A_dir( MOTOR_FORWARD, speed_motor_a - speed_motor_a/3);
      motor_B_dir( MOTOR_FORWARD, speed_motor_b);
	  break;
   case TrnLeftBwd_St:	// slow turn to left
      motor_A_dir( MOTOR_BACKWARD, speed_motor_a - speed_motor_a/3);
      motor_B_dir( MOTOR_BACKWARD, speed_motor_b);
	  break;
	  
    case Halt_St:		// halt state.  Check for push button to exit.
		motor_A_stop();
		motor_B_stop();
		autoMode = 0;
		if( lastState != state)
			Serial.println( F("Halt_St: ") );
		// house keeping on the halt state LED blinking
		if( currentTime > ledTogTime)
		{
			if(led2state == LOW)
			{	// set longer ON time for LED
				ledTogTime = currentTime + 800;
			}else
			{	// set short OFF time.
				ledTogTime = currentTime + 90;
			}
			led2state ^= 1;
			digitalWrite( LED2, led2state);
		}
		// always check kill switch
		if (checkKillSwitch())
		{
		  // start again   if kill switch.
		  nextState = Auto_Start_St;
		  digitalWrite( LED2, led2state);  // LED off.
		  led2state = LOW;
		  while (checkKillSwitch())
			;	// wait for user to get their greasy finger off the button.
		  delay(200);    // debounce time; ensure greasy finger completely off the switch.
		}
      break;
  }
    lastState = state;
}

// -----------------------------------------------
// Distance()
// Measure distance to object using ultrasonic sensor.
//
int Distance(void)
{
  float cm;
  int cm_int;
  digitalWrite(TrigPin, LOW); //Low, high and low level take a short time to TrigPin pulse
  delayMicroseconds(3);
  digitalWrite(TrigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(TrigPin, LOW);

  cm = pulseIn(EchoPin, HIGH) / 58.0; 	//Echo time conversion into cm
  cm = (int(cm * 100.0)) / 100.0; 		//Keep two decimal places
  if ( cm < 0) cm = 400;
  cm_int = (int) cm;
  return cm_int;
}

// ----------------------------------------------
// ReadMotorSkewAdjust()
//
//  Read and average motor skew adjustment
//  returns skew adjustment. Range 0 to 1023.
//
// -----------------------------------------------
#define INTEGRATE_NMBR_MSA (4) /* Readings to Average over */
unsigned int ReadMotorSkewAdjust(void)
{
  unsigned long rawSkew;
  lastSkewRawAvg *= INTEGRATE_NMBR_MSA;
  lastSkewRawAvg -= (lastSkewRawAvg / INTEGRATE_NMBR_MSA);	// average over 3 readings.
  //	if( lastSkewRawAvg < 0)
  //		lastSkewRawAvg = 0;
  // get reading 
  rawSkew = analogRead( MOTOR_SKEW );
  rawSkew = analogRead( MOTOR_SKEW );

  //		Serial.print(rawSkew);
  //		Serial.print(" ");
  //rawSkew /= (10 * INTEGRATE_NMBR_MSA);
  //		Serial.print(rawSkew);
  lastSkewRawAvg += rawSkew;
  //		Serial.print(" ");
  //		Serial.println(lastSkewRawAvg);
  lastSkewRawAvg /= INTEGRATE_NMBR_MSA;
  
  return lastSkewRawAvg;
}

// ----------------------------------------------
// BatteryVoltage()
//
//  Read and average battery voltage
//  returns battery voltage * 1000.
//
// @ 6.40v - still runs smooth, but VCC drops to 4.7v
// @ 5.00v - runs very slow.
// @ 6.66v - DX measurement unstable.
// -----------------------------------------------
#define INTEGRATE_NMBR (8) /* Readings to Average over */
unsigned int BatteryVoltage(void)
{
  unsigned long rawBattery;
  lastBattery -= (lastBattery / INTEGRATE_NMBR);	// average over X readings.
  //	if( lastBattery < 0)
  //		lastBattery = 0;
  // get reading (0-1023) and multiply by 2 due to voltage divider.
  rawBattery = analogRead( BATTERY_RAW );
  rawBattery = analogRead( BATTERY_RAW );

  //		Serial.print(rawBattery);
  //		Serial.print(" ");
  rawBattery *= (488 * 2);				// 4.88 mv/step  and *2 due to voltage divider resistors.
  rawBattery /= (100 * INTEGRATE_NMBR);
  //		Serial.print(rawBattery);
  lastBattery += rawBattery;
  //		Serial.print(" ");
  //		Serial.println(lastBattery);

  return lastBattery;
}
// -----------------------------------------------
// printStatus()
//
// Send the status string out the serial port.
//
// -----------------------------------------------
volatile ourstates lastStateStatus = Halt_St;	// used for status reporting on change
void printStatus(uint8_t force)
{
  static int lastDXreported = 0;
  static unsigned int lastBattReported = 0;
  static unsigned int lastSkewReported = 0;

  if ( !force  && state == lastStateStatus
       && lastDX <= (lastDXreported + 1) && lastDX >= (lastDXreported - 1)
       && lastBattery <= (lastBattReported + 50) && lastBattery >= (lastBattReported - 50)
	   && lastSkewRawAvg <= (lastSkewReported + 4) && lastSkewRawAvg >= (lastSkewReported - 4))
    return;  // not much to report.
  lastStateStatus = state;

  // report battery voltage
  Serial.print(F("vB: "));
  Serial.print((lastBattery / 10)+58);	// add 58 to compensate for 0.58 V diode drop. Vdrop is lower with higher current.
  lastBattReported = lastBattery;

  // report status of StateMachine state, turn count, DX, etc.
  Serial.print("\tST: ");
  Serial.print(state);
  

  Serial.print("\tTc: ");
  Serial.print(turnCount);

  //	Serial.print("\tEt: ");
  //	Serial.print(dirTogElapsedTime);
  
  // report Motor skew adjustment
  G_DEBUG(
  Serial.print(F("\tSK: "));
  Serial.print(lastSkewRawAvg);
  lastSkewReported = lastSkewRawAvg;
  Serial.print(F(" Ma: "));
  Serial.print(speed_motor_a);
  Serial.print(F(" Mb: "));
  Serial.print(speed_motor_b);
  
  );

  // report DX
  Serial.print(F("\tDX: "));
  Serial.print(lastDX);
  Serial.print(F("cm"));
  lastDXreported = lastDX;

  
  // End of this report...
  Serial.println();

}
