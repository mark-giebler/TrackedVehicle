#ifndef IR_HELI_LIB
#define IR_HELI_LIB
// =================================================================
// =================================================================
//
// 	IR RCVR hardware interface Library:
//
// Mark Giebler
//
//  V0.14 Tested OK: 2015-01-30
//  V0.15 Added get_ir_state():  2015-04-18
//  V0.16 Added Channel bits and methods:  2015-04-18
//
// ------------------------------------------------------------------
// We need to use the 'raw' pin reading methods
// because timing is very important here and the digitalRead()
// procedure is slower!
// Digital pin #11 is the same as Pin B3 see
// http://arduino.cc/en/Hacking/PinMapping168 for the 'raw' pin mapping.
// For PCINT3 type interrupt handling see:
//	https://sites.google.com/site/qeewiki/books/avr-guide/external-interrupts-on-the-atmega328
//	http://www.kriwanek.de/arduino/grundlagen/183-mehrere-pin-change-interrupts-verwenden.html
//info on AVR interrupts:
//  http://www.nongnu.org/avr-libc/user-manual/group__avr__interrupts.html
//
// bit pattern from Heli IR control device:
//
// A == bit 1 if on channel A
// B == bit 1 if on channel B
// C == if channel A & B bits are zero
// 		         B         A
// 0000 11111111 0 1011110 1 011 00000101  full throttle
//
// Time ---->
// MSB							  LSB 
// Throttle control:
// 		xxxxxxxx
// 0000 11111111 0 1011110 1 011 00000101  full throttle
// 0000 00000000 0 1011110 1 011 00000011  no throttle
//
// Direction control: fwd/rev  
//						   A xxx
// 0000 10001111 0 1011110 1 111 00001000		full fwd
// 0000 01111011 0 1011110 1 011 00000001		center
// 0000 10001110 0 1011110 1 000 00001111		full rev
//
// Direction control: left/right (trim was offset from mid-range.)
//				   xxxxxxx A
// 0000 10010101 0 1111111 1 011 00000010		full left
// 0000 10010101 0 0101101 1 011 00001001		center
// 0000 10010101 0 0011111 1 011 00001000		full right
//
// Examples:
// vary trim  with stick in center.  
// 0000 10100010 0 0011111 1 011 00001010  		trim right
// 0000 10100010 0 1001000 1 011 00001110 		trim left
//
// Chn A, trim center, stick center, throttle center:
// 0000 01111001 0 0111111 1 011 00000100
// 0000 01111001 0 1000000 1 011 00000010
//
// Decoded controls captured:
// 0:1244 31:99452 IR Thr:0 F/R:3 Dir:123   Fwd/Rev - Centered, Direction - Centered
// 0:1232 31:99476 IR Thr:16 F/R:3 Dir:1    Direction: Full Right.
// 0:1228 31:99456 IR Thr:16 F/R:3 Dir:251	 Direction: Full Left
// 0:1228 31:99464 IR Thr:16 F/R:7 Dir:123  Full Fwd 
// 0:1228 31:99464 IR Thr:16 F/R:0 Dir:123  Full Rev 
//
// 0:1240 31:52389124 IR Thr:11 F/R:3 Dir:117	After power cycle, direction center is shifted lower.
//
// Typical gap times between commands: in uSec
// 99260
// 99236
// 99292
// 98448
//
//------------------------------------------------
// define bit field structure for the IR data.
// Remember, that struct for AVR gcc is bass-ackwards. 
// You want the most-significant fields on the bottom, rather than the top, so the bits fill them in properly.
struct Stc_ir_cmd
{
 uint8_t 	n3_fill:8;			// LSB
 uint8_t 	b_fwdrev:3;
 uint8_t 	b_channelA:1;		// 1 == channel A sending
 uint8_t 	b_direction:7;		// 
 uint8_t 	b_channelB:1;		// 1 == channel B sending
 uint8_t 	b_throttle:8;
								// upper 4 bits for status, not from IR sender.
 uint8_t	b_busy:1;
 uint8_t	b_idle:1;
 uint8_t	b_paused:1;
 uint8_t 	b_error:1;			// MSB
};
struct Stc_ir_dump
{
	uint8_t b0:8;
	uint8_t b1:8;
	uint8_t b2:8;
	uint8_t b3:8;	// MSB
};
union Ir_Data
{
	uint32_t raw;
	Stc_ir_cmd cmds;
	Stc_ir_dump bytes;
} u_ir_cmd;

uint8_t get_byte0()
{
	return u_ir_cmd.bytes.b0;
}
uint8_t get_byte1()
{
	return u_ir_cmd.bytes.b1;
}
uint8_t get_byte2()
{
	return u_ir_cmd.bytes.b2;
}
uint8_t get_byte3()
{
	return u_ir_cmd.bytes.b3;
}

// -----------------------------------------------------
// Mark's debug macro
// -----------------------------------------------------
// 		mjg: put any debug only code inside the parenthesis (represented by 'a') in below macro.  
//  	'a' can be multiple statements on multiple lines.
// 		comment out next line to turn off all debug code.  
#define G_DEBUG(a)  a;
#ifndef G_DEBUG
// 		if G_DEBUG() is commented out above, then define NULL debug macro that throws code 'a' away.
#define G_DEBUG(a) ;;;
#endif
// 		use this macro to temporarily disable an individual G_DEBUG() macro group of code by renaming it to this:
#define G_DEBUG_N(a) ;;;
// -----------------------------------------------------
// END -- Mark's debug macro
// -----------------------------------------------------

#define IRpin_PORT PINB  // not a pin, but port B, need to mask bit to get at pin 11 by direct read.
#define IRpin_BIT	3	// bit 3 of port B is arduino pin 11
#define IRpin 11		// arduino pin #
#define IRtestPin	19	// AN5 - for testing ISR

// added these includes 2015-01-07
#include <avr/interrupt.h>
#include <avr/io.h>


// -----------------------------------------------------
// -----------------------------------------------------
// -----------------------------------------------------
// Define IR protocol related variables.
enum ir_states
{
	WaitHeaderStart,
	WaitHeaderBit1,
	WaitHeaderBit2,
	grabBits,
	commandDone,
	framingError,
	irPaused
};
ir_states ir_state = irPaused;

// define pulse width timings for header bits, 0 bits and 1 bits.
// 0 bit range:  648  -  908
// 1 bit range:  1444, 1460 - 1644 , 1656

#define PULSE_WIDTH_HEADER			(1220ul)	// 1.24 mSec
#define PULSE_WIDTH_ZERO_BIT		( 760ul)	// should be  670 uSec  but arduino timing seems way off.
#define PULSE_WIDTH_ONE_BIT			(1552ul)	// 1.52 mSec
#define PULSE_WIDTH_HEAD_TOLERANCE	( 85ul)		//  tolerance on pulse widths.
#define PULSE_WIDTH_ZERO_TOLERANCE	( 125ul)	// tolerance on zero bit pulse widths. lots of jitter on zero bits. seen 908 & 648 uSec
#define PULSE_WIDTH_ONE_TOLERANCE	( 100ul)	//  tolerance on pulse widths. seen 1444 

#define IR_DATA_BITS	(28)				// number of bits to grab in ISR and to decode.
// the maximum IR pulse we'll listen for - give time of > 3 the cmd time (42Sec)  + gap time (135mSec)
#define PULSE_TIMEOUT_MAX 		(3ul * (4200ul + 135000ul))
#define PULSE_WIDTH_MAX 		(3ul * PULSE_WIDTH_ONE_BIT)

// time stamp of the last falling edge, if greater than MAXPULSE then restart state machine.
volatile  uint32_t lastFallTime_usec = 0;	// set by ISR
volatile  uint32_t irStartTime_usec = 0;	// set by irStart() - not used currently.
volatile  uint32_t irPulseLen_usec;			// Used by ISR to determine length of pulse sequence for one bit. 
uint32_t  irBitTimes[32];					// length of time for each bit. [0] - has header bit 1 time, [2] has MSB
// ir_bit - contains which bit is being received.  Set to initial value by ISR when stateMachine enters WaitHeaderBit1.
uint8_t ir_bit = 0;	// The protocol has 28 bits, MSB sent first.


// ---------------------------------------------------
// ISR (PCINT0_vect)
//
//  IR hardware interrupt service routine
//  this is called each time the IR input pin changes
//  from logic state (HIGH to logic LOW or LOW to HIGH). 
//	The Falling edge is what we care about.
//  It implements the IR bit time grabbing state machine.
//  each bit received is stuffed into the array
//  irBitTimes[] 
//  [0] - has header bit 1 time
//  [1] - is header bit 2 time
//  [2] - is MSB bit time
//
//
volatile uint32_t timeNow_usec;
volatile uint8_t changedBits = 0x5a;
volatile uint8_t portHistory = 0xFF;     // default is high because the pull-up
volatile uint8_t portNow = 0xff;

ISR( PCINT0_vect )
{
	portNow = IRpin_PORT;					// read the port the IR sensor is connected to.
    changedBits = portNow ^ portHistory; 	// find out what pin changed.
    portHistory = portNow;

	G_DEBUG_N( digitalWrite(IRtestPin, HIGH); );
	
	if( (changedBits & (1 << IRpin_BIT)) &&  !(portNow & (1 << IRpin_BIT)) )
	{	//  IR input pin changed, and falling edge.
			
		timeNow_usec = micros();
		irPulseLen_usec = timeNow_usec - lastFallTime_usec;
		
		switch(ir_state)
		{
			case WaitHeaderStart:
				ir_state = WaitHeaderBit1;		// move to next state.
				
				//irBitTimes[0] = 0;				// clear header bit 1 time.
				irBitTimes[31] = irPulseLen_usec;			// save gap between commands time. For debug info.
			break;
			
			case WaitHeaderBit1:
				ir_bit = 0;
				// check if header bit 1 timing constraint was met.
				if( irPulseLen_usec < (PULSE_WIDTH_HEADER+PULSE_WIDTH_HEAD_TOLERANCE) && irPulseLen_usec > (PULSE_WIDTH_HEADER-PULSE_WIDTH_HEAD_TOLERANCE) )
				{
					// have valid header bit 1, go look for 2.
					ir_state = WaitHeaderBit2;
				}else
				{
					// not a valid header, keep waiting here (v0.08 changed from WaitHeaderStart)
					ir_state = WaitHeaderBit1;
				}
				irBitTimes[ir_bit++] = irPulseLen_usec;
			
			break;
			
			case WaitHeaderBit2:
				// check if header bit 2 timing constraint was met.
				if( irPulseLen_usec < (PULSE_WIDTH_HEADER+PULSE_WIDTH_HEAD_TOLERANCE) && irPulseLen_usec > (PULSE_WIDTH_HEADER-PULSE_WIDTH_HEAD_TOLERANCE) )
				{
					// have valid header bit 1, go grab the bits that will follow.
					ir_state = grabBits;
				}else
				{
					// not a valid header, (v0.08 changed from WaitHeaderStart) was: back to WaitHeaderStart state
					ir_state = WaitHeaderBit1;
				}
				irBitTimes[ir_bit++] = irPulseLen_usec;
			break;
			
			case grabBits:
				irBitTimes[ir_bit++] = irPulseLen_usec;
				if( ir_bit > (IR_DATA_BITS+1))
					ir_state = commandDone;			// all 28 data bits + 2 header bits rcvd.
				if( irPulseLen_usec > PULSE_WIDTH_MAX)	//  framing error on last bit (28) is higher priority.
				{
					ir_state = framingError;		// framing error.  Todo:  send this back to WaitHeaderBit1
					ir_bit--;						// point back at bit that caused framingError.
				}
			break;
			
			case commandDone:	// do nothing....
			case irPaused:
			case framingError:
				timeNow_usec = lastFallTime_usec;			// don't mess with lastFallTime_usec on exit.
				G_DEBUG_N( isr_falling_count--;);				// adjust back down one.
			break;		
		
			default:	// state machine is in an invalid state.
				ir_state = WaitHeaderStart;
			break;
			
		}
		lastFallTime_usec = timeNow_usec;
	}
	G_DEBUG_N( digitalWrite(IRtestPin, LOW); );

}

// ---------------------------------------------------------------------
// ir_processCommand()
//
// decodes the bit times in irBitTimes[] into
// one 28 bit command word, Stored in u_ir_cmd.raw
//
// Uses global pulse time array irBitTimes[]
//	[0] is header bit 1 time
//	[1] is header bit 2 time
//	[2 .. 30]  are data bit times.
//	[31]  is inter-command gap time between this command and previous one.
//
// Updates global structure u_ir_cmd.raw
//
//  Returns:
//		(if no errors)
//		28 bit command code  0 to 0x3fffffff (value less than 0x40000000  IR_BUSY)
//		(if IR receive in progress)
//  	0x40000000  IR_BUSY      if IR command still being received.
//
//  	if error:  (state machine will be restarted)
// 		 0xffffffff	 IR_INVALID  if invalid bit times encountered
//		 0xfffffffe  IR_FRAMING  if partial bits received but last falling edge was more than 65 mSec ago, framing error.
//
#define IR_STATUS_MASK (0xF0000000ul)
#define IR_DATA_MASK	(~IR_STATUS_MASK)
#define IR_BUSY		(0x10000000ul)  // values less than this are valid commands.
#define IR_IDLE		(0x20000000ul)	// idle, waiting for header to start.
#define IR_PAUSED	(0x40000000ul)	// irPaused.
#define IR_ERROR	(0x80000000ul)  // high bit set indicates an error.
#define IR_FRAMING_ISR (0x8ffffffdul) // isr detected framing error.
#define IR_FRAMING	(0x8ffffffeul)	// framing error - timeout waiting for isr to finish.
#define IR_INVALID	(0x8ffffffful)	// invalid bit time
#define IR_FAIL		(IR_ERROR | 1ul) // state machine or logic failure.
ir_states ir_state_t = WaitHeaderStart;			// used to grab a copy so don't change by ISR.

uint32_t ir_processCommand()
{
	uint32_t bitTime;
	uint8_t tmBit;						// start @ MSB time (skip header bit times)
	uint8_t cmdBit;						// start @ MSB
	ir_state_t = ir_state;				// grab copy so won't change by ISR.
	u_ir_cmd.raw = IR_IDLE;					// assume idle 
	// first check for various errors.
	if(ir_state_t == framingError)
	{
		u_ir_cmd.raw = IR_FRAMING_ISR;		// return ISR framing error code.
	}else if( ir_state_t != commandDone )
	{
		 
		{	// no errors and idle or command still in progress.
			if( ir_state_t == WaitHeaderStart )
			{
				return u_ir_cmd.raw;
			}else if( ir_state_t == irPaused )
			{
				return IR_PAUSED;
			}
			return IR_BUSY;
		}
	}
	
	if( ir_state_t == commandDone )
	{
		cmdBit = IR_DATA_BITS;		// start @ MSB
		tmBit = 2;					// start @ MSB time (skip header bit times)
		u_ir_cmd.raw = 0;
		while( cmdBit != 0)
		{
			bitTime = irBitTimes[tmBit++];
			
			if( bitTime < (PULSE_WIDTH_ZERO_BIT + PULSE_WIDTH_ZERO_TOLERANCE) && bitTime > (PULSE_WIDTH_ZERO_BIT - PULSE_WIDTH_ZERO_TOLERANCE))
			{ // 0 bit
				cmdBit--;	// bit already zero in command accumulator
			}else if( bitTime < (PULSE_WIDTH_ONE_BIT + PULSE_WIDTH_ONE_TOLERANCE) && bitTime > (PULSE_WIDTH_ONE_BIT - PULSE_WIDTH_ONE_TOLERANCE) )
			{	// 1 bit
				cmdBit--;
				u_ir_cmd.raw |= (1ul<<cmdBit);
				
			}else 
			{	// invalid bit time
				cmdBit = 0;	// exit while loop.
				u_ir_cmd.raw = IR_INVALID;
				G_DEBUG(
					Serial.print(F(" errorBit: "));
					Serial.print(tmBit-1);
					Serial.print(F(" time:"));
					Serial.print(irBitTimes[tmBit-1]);
					Serial.println();
				)
			}
		}
	}
	return u_ir_cmd.raw;
}

// *********************************************************************
// ir status command below only valid AFTER a call to ir_processCommand()
// *********************************************************************

// return if command ready
// 1 == valid command data ready
//
uint8_t ir_ready()
{
	if( u_ir_cmd.raw & IR_STATUS_MASK )
	{
		return 0;	// no valid command yet...
	}	
	return 1;		//  valid command.	
}
// return busy status.
// 1 == busy
uint8_t ir_busy()
{
	return u_ir_cmd.cmds.b_busy;
}

// return idle status.
// 1 == idle
uint8_t ir_idle()
{
	return u_ir_cmd.cmds.b_idle;
}

// return paused status.
// 1 == paused
uint8_t ir_paused()
{
	return u_ir_cmd.cmds.b_paused;
}

// return error status.
// 1 == error
uint8_t ir_error()
{
	return u_ir_cmd.cmds.b_error;
}

// return ir state machine state
uint8_t get_ir_state(void)
{
    return ir_state;
}
// ************************************
// The throttle and vector calls below are only valid if
// ir_ready() returns True.
// ************************************

// Throttle:  Range 0 to 255 (max)
uint8_t get_throttle()
{
	return u_ir_cmd.cmds.b_throttle;
}

// Fwd: 4 - 7
// Center:  3
// Rev: 0 - 2
uint8_t get_Xvector()
{
	return u_ir_cmd.cmds.b_fwdrev;
}

// With trim at center:
// Right:   0 to 55
// Center:  56 to 70  (63 +/- 7)
// Left: 	71 to 127
uint8_t get_Yvector()
{
	return u_ir_cmd.cmds.b_direction;
}


uint8_t get_channelA()
{
	return u_ir_cmd.cmds.b_channelA;
}

uint8_t get_channelB()
{
	return u_ir_cmd.cmds.b_channelB;
}

uint8_t get_channelC()
{
	return ( !u_ir_cmd.cmds.b_channelA && !u_ir_cmd.cmds.b_channelB);
}
// Channel return values:
// 1 == A
// 2 == B
// 3 == C
uint8_t get_channel()
{
	if( u_ir_cmd.cmds.b_channelA )
	{
		return 1;
	}
	if( u_ir_cmd.cmds.b_channelB )
	{
		return 2;
	}
	return 3;	// C
}

// --------------------------------------
// protected access to irStartTime_usec
uint32_t get_irStartTime_usec()
{
	uint32_t val;
	uint8_t SaveSREG = SREG;   	// save interrupt flag
    noInterrupts();   			// disable interrupts
    val = irStartTime_usec;  	// access the shared data
    SREG = SaveSREG;   			// restore the interrupt flag
	return val;
}
// protected access to lastFallTime_usec
uint32_t get_lastFallTime_usec()
{
	uint32_t val;
	uint8_t SaveSREG = SREG;   	// save interrupt flag
    noInterrupts();   			// disable interrupts
    val = lastFallTime_usec;  	// access the shared data
    SREG = SaveSREG;   			// restore the interrupt flag
	return val;
}
	

// =================================================================
// =================================================================
// cooperative multi-tasking Time delay timer
//
uint32_t timerEnd = 0;	// 0 == no timer running, if Non-zero, timer is running.

void timerClear()
{
	timerEnd = 0;	// clear the counter.
}

// ---------------------------------------
// timerMsec()
//  if no timer running, start a new elapsed timer 
//  else check if previous timer has expired.
//
// input: delay  
//			the desired elapsed time in mSec to start if not already running. 
//				If timer currently running value is ignored.
//			0 - to only check if previous timer expired (and not start a new timer)
// 
// return:  mSecs remaining before expires.  0 - if time has expired.
// 
uint32_t timerMsec(uint32_t delay)
{
	if( timerEnd == 0 && delay != 0 )
	{	// no timer running, so start one.
		timerEnd = millis() + delay;
	}
	return timerRemaining();
}

uint32_t timerRemaining()
{
	// return the amount of time (mSec) remaining in the timer. 0 if timer expired.
	uint32_t now = millis();
	if( timerEnd == 0 || now > timerEnd )
	{
		timerClear();	// clear the counter.
		return 0;		// timer expired.
	}
	return(timerEnd - now);
}

void timerStart(uint32_t delay)
{
	// only start a new delay timer if previous one is not running.
	if( timerEnd == 0)
		timerEnd = millis() + delay;
}

uint8_t timerExpired()
{
	// check if a running timer timer has expired, 1 == expired, 0 == not expired.
	if(timerRemaining() == 0)
	{
		return 1;
	}
	return 0;
}


// ---------------------------------------------------
// ---------------------------------------------------
// ir_setup()
//
//  Call this from within the arduino setup() function.
// ---------------------------------------------------
// ---------------------------------------------------
void ir_setup()
{
	// initialize state machine
	ir_startRx();
	
	// initialize hardware interface
	// Make Arduino Pin 11 (PCINT3/Port B.3) an input and set pull up resistor:
	pinMode( IRpin, INPUT_PULLUP );
	
G_DEBUG_N(
	pinMode( IRtestPin, OUTPUT);	// for toggling in the ISR view with scope.
	digitalWrite(IRtestPin, LOW); 
)
	
	// setup Pin Change Interrupt registers
	PCICR |= (1 << PCIE0);    // set PCIE0 to enable PCMSK0 scan
    PCMSK0 |= (1 << PCINT3);  // set PCINT3 Pin 11 to trigger an interrupt on state change 
	interrupts();             // Enable interrupts  Do we need this?  TODO: Test without it to see i ints start happening again.
}

void ir_startRx()
{
	// prepare state machine to RX another IR stream.
	ir_state = irPaused;	// pause the ISR state machine
	u_ir_cmd.raw &= IR_DATA_MASK;	// turn off status bits, keep any data.
	u_ir_cmd.cmds.b_idle = 1;  	// set to idle status.
	irPulseLen_usec = 0;	// set to 0 so can detect first firing of ISR.
	irBitTimes[0] = 1;
	irBitTimes[31] = 0;
	irStartTime_usec = lastFallTime_usec = micros();
	ir_state = WaitHeaderStart;		// start the ISR
}

// =========================================================
//
//  end of library
//
// =========================================================
#endif

